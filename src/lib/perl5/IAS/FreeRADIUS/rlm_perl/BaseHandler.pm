#!/usr/bin/perl

package IAS::FreeRADIUS::rlm_perl::BaseHandler;

use strict;
use warnings;
=pod

=head1 NAME

IAS::FreeRADIUS::rlm_perl::BaseHandler

=head1 SYNOPSIS

  use strict;
  use warnings;
  
  use IAS::FreeRADIUS::rlm_perl::BaseHandler;
  use base 'IAS::FreeRADIUS::rlm_perl::BaseHandler';

  sub authenticate
  {
  	my ($self) = @_;
	
  	# $self->log_request_attributes(); # Debug only!!
  	return RLM_MODULE_OK;
  }

=head1 DESCRIPTION

This couples with an entry point script for FreeRADIUS, currently named "rlm_perl_entry.pl".



=over 4

=item https://wiki.freeradius.org/modules/Rlm_perl

=back

=head1 An Example Debug Message

  (0) perl:   $RAD_REQUEST{'User-Name'} = &request:User-Name -> 'user'
  (0) perl:   $RAD_REQUEST{'User-Password'} = &request:User-Password -> 'passwd'
  (0) perl:   $RAD_REQUEST{'NAS-IP-Address'} = &request:NAS-IP-Address -> '127.0.1.1'
  (0) perl:   $RAD_REQUEST{'NAS-Port'} = &request:NAS-Port -> '18120'
  (0) perl:   $RAD_REQUEST{'Event-Timestamp'} = &request:Event-Timestamp -> 'Dec 30 2019 19:51:49 EST'
  (0) perl:   $RAD_REQUEST{'Message-Authenticator'} = &request:Message-Authenticator -> '0x74ba4bd2663e5bbf32860d8bde90d0e1'
  (0) perl:   $RAD_CHECK{'Auth-Type'} = &control:Auth-Type -> 'Perl'
  (0) perl:   $RAD_CONFIG{'Auth-Type'} = &control:Auth-Type -> 'Perl'
  (0) perl: &request:NAS-Port = $RAD_REQUEST{'NAS-Port'} -> '18120'
  (0) perl: &request:Event-Timestamp = $RAD_REQUEST{'Event-Timestamp'} -> 'Dec 30 2019 19:51:49 EST'
  (0) perl: &request:User-Password = $RAD_REQUEST{'User-Password'} -> 'passwd'
  (0) perl: &request:Message-Authenticator = $RAD_REQUEST{'Message-Authenticator'} -> '0x74ba4bd2663e5bbf32860d8bde90d0e1'
  (0) perl: &request:NAS-IP-Address = $RAD_REQUEST{'NAS-IP-Address'} -> '127.0.1.1'
  (0) perl: &request:User-Name = $RAD_REQUEST{'User-Name'} -> 'user'
  (0) perl: &control:Auth-Type = $RAD_CHECK{'Auth-Type'} -> 'Perl'


=cut

require Exporter;
our @ISA = qw(Exporter);
our @EXPORT_OK;
our @EXPORT;

use constant {
	RLM_MODULE_REJECT   =>  0, #  immediately reject the request
	RLM_MODULE_FAIL     =>  1, #  module failed, don't reply
	RLM_MODULE_OK       =>  2, #  the module is OK, continue
	RLM_MODULE_HANDLED  =>  3, #  the module handled the request, so stop.
	RLM_MODULE_INVALID  =>  4, #  the module considers the request invalid.
	RLM_MODULE_USERLOCK =>  5, #  reject the request (user is locked out)
	RLM_MODULE_NOTFOUND =>  6, #  user not found
	RLM_MODULE_NOOP     =>  7, #  module succeeded without doing anything
	RLM_MODULE_UPDATED  =>  8, #  OK (pairs modified)
	RLM_MODULE_NUMCODES =>  9, #  How many return codes there are
};

our %RLM_MODULE_RETURN_CONSTANTS_REVERSE_MAP = reverse (
	'RLM_MODULE_REJECT'   =>  0,
	'RLM_MODULE_FAIL'     =>  1,
	'RLM_MODULE_OK'       =>  2,
	'RLM_MODULE_HANDLED'  =>  3,
	'RLM_MODULE_INVALID'  =>  4,
	'RLM_MODULE_USERLOCK' =>  5,
	'RLM_MODULE_NOTFOUND' =>  6,
	'RLM_MODULE_NOOP'     =>  7,
	'RLM_MODULE_UPDATED'  =>  8,
	'RLM_MODULE_NUMCODES' =>  9,
);

sub decode_rlm_module_return_code
{
	my ($self, $code) = @_;
	return $RLM_MODULE_RETURN_CONSTANTS_REVERSE_MAP{$code}
}
use constant {
	L_AUTH	 => 2,  # Authentication message
	L_INFO	 => 3,  # Informational message
	L_ERR	  => 4,  # Error message
	L_WARN	 => 5,  # Warning
	L_PROXY	=> 6,  # Proxy messages
	L_ACCT	 => 7,  # Accounting messages
	L_DBG	  => 16, # Only displayed when debugging is enabled
	L_DBG_WARN     => 17, # Warning only displayed when debugging is enabled
	L_DBG_ERR      => 18, # Error only displayed when debugging is enabled
	L_DBG_WARN_REQ => 19, # Less severe warning only displayed when debugging is enabled
	L_DBG_ERR_REQ  => 20, # Less severe error only displayed when debugging is enabled
};


push @EXPORT, qw(
	RLM_MODULE_REJECT
	RLM_MODULE_FAIL
	RLM_MODULE_OK
	RLM_MODULE_HANDLED
	RLM_MODULE_INVALID
	RLM_MODULE_USERLOCK
	RLM_MODULE_NOTFOUND
	RLM_MODULE_NOOP
	RLM_MODULE_UPDATED
	RLM_MODULE_NUMCODES

	L_AUTH
	L_INFO
	L_ERR
	L_WARN
	L_PROXY
	L_ACCT
	L_DBG
	L_DBG_WARN
	L_DBG_ERR
	L_DBG_WARN_REQ
	L_DBG_ERR_REQ
);

sub new
{
	my $type = shift;
	my $self = shift // {};
	return bless $self, $type;
}

sub authorize {
	# log_request_attributes(); # debug
	return RLM_MODULE_OK;
}
sub authenticate {
	# log_request_attributes(); # debug

	#if ($RAD_REQUEST{'User-Name'} =~ /^baduser/i) {
	#	# Reject user and tell him why
	#	$RAD_REPLY{'Reply-Message'} = "Denied access by rlm_perl function";
	#	return RLM_MODULE_REJECT;
	#} else {
	#	# Accept user and set some attribute
	#	if (&radiusd::xlat("%{client:group}") eq 'UltraAllInclusive') {
	#		# User called from NAS with unlim plan set, set higher limits
	#		$RAD_REPLY{'h323-credit-amount'} = "1000000";
	#	} else {
	#		$RAD_REPLY{'h323-credit-amount'} = "100";
	#	}
	#	return RLM_MODULE_OK;
	#}

	
	return RLM_MODULE_REJECT;
}

sub preacct {
	# log_request_attributes(); # debug

	return RLM_MODULE_OK;
}

sub accounting {
	# log_request_attributes(); # debug
	return RLM_MODULE_OK;
}

sub checksimul {
	# log_request_attributes(); # debug
	return RLM_MODULE_OK;
}
sub pre_proxy {
	# log_request_attributes(); # debug
	return RLM_MODULE_OK;
}

sub post_proxy {
	# log_request_attributes(); # debug
	return RLM_MODULE_OK;
}

# Function to handle post_auth
sub post_auth {
	# log_request_attributes(); # debug
	return RLM_MODULE_OK;
}

sub xlat {
	# log_request_attributes(); # debug

	# Loads some external perl and evaluate it
	# my ($filename,$a,$b,$c,$d) = @_;
	# &radiusd::radlog(L_DBG, "From xlat $filename ");
	# &radiusd::radlog(L_DBG,"From xlat $a $b $c $d ");
	# local *FH;
	# open FH, $filename or die "open '$filename' $!";
	# local($/) = undef;
	# my $sub = <FH>;
	# close FH;
	# my $eval = qq{ sub handler{ $sub;} };
	# eval $eval;
	# eval {main->handler;};

}

sub detach {
	# log_request_attributes(); # debug

}

sub log_request_attributes {
	# This shouldn't be done in production environments!
	# This is only meant for debugging!
	my ($self) = @_;
	
	&radiusd::radlog(L_DBG, "+++++log_request_attributes:+++++");
	for my $key (keys %{$self->{RAD}->{REQUEST}}) {
		&radiusd::radlog(
			L_DBG,
			"REQUEST: $key = "
			. $self->{RAD}->{REQUEST}->{$key}
		);
	}
}

push @EXPORT_OK, (
	'authorize',
	'authenticate',
	'preacct',
	'accounting',
	'checksimul',
	'pre_proxy',
	'post_proxy',
	'post_auth',
	'xlat',
	'detach',

	'log_request_attributes',	
);

1;
