#!/usr/bin/perl

package IAS::FreeRADIUS::rlm_perl::A;

use strict;
use warnings;

=pod

This is currently a test module for implementing IAS::FreeRADIUS::rlm_perl::BaseHandler
style processing.

The examples in here might prove useful, but overall, documentation of the
functional, abstracted, and "supported" functionality should be in BaseHandler.pm

=cut

# Something I thought was potentially a bad idea, but pretty nifty:
# use File::Basename;
# use lib dirname(__FILE__).'/'. ('../' x (scalar (split('::',__PACKAGE__)) -1));

use IAS::FreeRADIUS::rlm_perl::BaseHandler;
use base 'IAS::FreeRADIUS::rlm_perl::BaseHandler';

sub authenticate
{
	my ($self) = @_;
	
	# $self->log_request_attributes(); # Debug only!
	return RLM_MODULE_OK;
}

sub say_hello
{
	my ($self) = @_;
	
	print "Hello, from ", __PACKAGE__, $/;
}


1;
