#!/usr/bin/perl

use strict;
use warnings;

=pod

=head1 NAME

rlm_perl_entry.pl

=head1 SYNOPSIS

=head2 FreeRADIUS Module

In a "perl" style FreeRADIUS module configuration:

  perl {
  	filename = /opt/IAS/bin/ias-freeradius-rlm-perl-base/rlm_perl_entry.pl
  	
  	# For example:
  	config {
  		handler_module = "IAS::FreeRADIUS::rlm_perl::A"
  		# For development:
  		# If you have /home/marty/junk/galahad/IAS/FreeRADIUS/rlm_perl/Galahad.pm
  		lib_dir = "/home/marty/junk/galahad"
  	}
  }
  
Where "IAS::FreeRADIUS::rlm_perl::A" is your module, which follows the directions
in IAS::FreeRADIUS::BaseHandler.

=head2 Variables

The data stored in $radius_data (see source) is provided to your module
via $self->{RAD}->{REQUEST} (for example).

=head1 DESCRIPTION

Provides an "entry point" for rlm_perl style FreeRADIUS requests.

The FreeRADIUS rlm_perl module calls everything as if it's in the "main"
namespace.  This can be problematic for the following reasons:

=over 4

=item * Constants specified in the documentation have to either be copied
and pasted into each authentication script, or they have to be "required"
somehow.

=item * Redefining empty, or unused functinality makes every rlm_perl
script have redundant code in it

=back

This setup "solves" these problems by:

=over 4

=item * Loading the module specified in $RAD_PERLCONF{'handler_module'}

=item * "Rewriting" calls to main::<something> to YourModule::something as an object

=item * Providing "base" functionality in IAS::FreeRADIUS::rlm_perl::BaseHandler

=item * Providing exported constants in IAS::FreeRADIUS::rlm_perl::BaseHandler

=back

Another result of this is you can have a test script "require" rlm_perl_entry.pl ,
set variables in that, and then call things like authenticate() without needing
 access to a FreeRADIUS server to develop with.

=cut

use Cwd;
use File::Basename;
my $RealPath;
BEGIN {
        
        $RealPath = Cwd::realpath(__FILE__);
}

use lib '/opt/IAS/lib/perl5';
use lib dirname($RealPath).'/../lib/perl5';

use Module::Runtime qw(use_module);
use Data::Dumper;

use vars (
	'%RAD_REQUEST', # Attributes from the request
	'%RAD_REPLY', # Attributes for the reply

	'%RAD_CHECK', # Check items

	'%RAD_REQUEST_PROXY', # Attributes from the proxied request
	'%RAD_REQUEST_PROXY_REPLY', # Attributes from the proxy reply

	'%RAD_STATE', # Session state
	
	'%RAD_PERLCONF',
);

$RAD_PERLCONF{'handler_module'} //= 'IAS::FreeRADIUS::rlm_perl::BaseHandler';

my $radius_data = {
	'REQUEST' => \%RAD_REQUEST,
	'REPLY' => \%RAD_REPLY,

	'CHECK' => \%RAD_CHECK,

	'REQUEST_PROXY' => \%RAD_REQUEST_PROXY,
	'REQUEST_PROXY_REPLY' => \%RAD_REQUEST_PROXY_REPLY,

	'STATE' => \%RAD_STATE,
	
	'PERLCONF' => \%RAD_PERLCONF,
};

if ($radius_data->{'PERLCONF'}->{'lib_dir'})
{
	push @INC, $radius_data->{'PERLCONF'}->{'lib_dir'};
}

our $handler;

sub AUTOLOAD {
	our $AUTOLOAD;
	my $new_name = $AUTOLOAD;
	$new_name =~ s/^main:://;
	
	$handler = use_module($radius_data->{'PERLCONF'}->{'handler_module'})->new({
		'RAD' => $radius_data,
	});

	if (! $handler->can($new_name))
	{
		warn $RAD_PERLCONF{'handler_module'} . " can't $new_name ";
	}
	else
	{
		return $handler->$new_name();
	}	
}

sub get_main_rlm_handler
{
	return $handler;
}

sub get_main_rlm_radius_data
{
	return $radius_data;
}

1;
