#!/usr/bin/perl

use strict;
use warnings;

# If you want to use this for testing,
# Copy and paste starting here:
use Cwd;
use File::Basename;
use Data::Dumper;

my $RealPath;
BEGIN {
	$RealPath = Cwd::realpath(__FILE__);
}

use lib '/opt/IAS/lib/perl5';
use lib dirname($RealPath).'/../bin/';
use lib dirname($RealPath).'/../lib/perl5';

require "rlm_perl_entry.pl";

use IAS::FreeRADIUS::rlm_perl::BaseHandler;

my $radius_data = get_main_rlm_radius_data();

# ENDING here

# Put your module name here:
$radius_data->{'PERLCONF'}->{'handler_module'} = 'IAS::FreeRADIUS::rlm_perl::A';

say_hello();
my $result = authenticate();
# This has to go AFTER the first call:
my $handler = get_main_rlm_handler();

# Some debug code can go here:

printf STDOUT
	"Got %s from authenticate()\n",
	$handler->decode_rlm_module_return_code($result),
;
